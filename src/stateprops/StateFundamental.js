import React, {Fragment, Component } from 'react'
import { Text, StyleSheet, View, Button, TouchableOpacity } from 'react-native'
import { TapGestureHandler, TextInput } from 'react-native-gesture-handler'
 
class StateFundamental extends Component {
    constructor(){
        super()
        this.state = {
            id:'',
            nama: '',
            address: '',
            nilai : 90,
            adik: [
                {id:1, nama: 'Akmil', alamat: 'tanggerang'},
                {id:2, nama: 'ikmal', alamat: 'tanggerang'},
            ]
        }
    }
 
    handleChangeName = () => {
        this.setState({
            nama: 'Eca'
        })
    }
 
    handleAddData = () => {
        let newData = {
            id : this.state.adik.length + 1,
            nama: this.state.nama,
            alamat: this.state.address
        }
 
        this.setState({
            adik : [...this.state.adik, newData]
        })
    }
 
    handleInputData = inputType => e => {
        this.setState({
            [inputType]: e
        })
    }

    handleDeleteData = (id) => {
      this.setState({
        adik: this.state.adik.filter(adik => adik.id != id)
      })
    }

    handleEditData = (id, nama, alamat) => {
      this.setState({
        id: id,
        nama: nama,
        address: alamat
      })
    }
 
    updateData = () => {
      const newDataMap = this.state.adik.map(item => {
        if(item.id == this.state.id){
          return {
            id: this.state.id,
            nama: this.state.nama,
            alamat: this.state.address
          }
        }
        return item
      })
      this.setState({
        adik : newDataMap
      })
    }

    render() {
        const {id, nama, address, adik} = this.state
        return (
            <View>
                <Text> State & Props Fundamental </Text>
                <Text>Id : {id}</Text>
                <Text>Nama : {nama}</Text>
                <Text>Alamat : {address}</Text>
                <Button title='Change' onPress={this.handleChangeName} />
                {adik.map(item => {
                    const {nama, id} = item
                    return <Text key={id}>{nama}</Text>
                })}
 
                <Text>-------------------------------------------------------</Text>
 
                <TextInput value={nama} placeholder='Nama' onChangeText={this.handleInputData('nama')} style={{borderWidth:1}} />
                <TextInput value={address} placeholder='Alamat' onChangeText={this.handleInputData('address')} style={{borderWidth:1}} />
                <Button title='Add Data' onPress={this.handleAddData} />
                <Button title='Update Data' onPress={this.updateData} />
 
                <Text style={{fontWeight: 'bold', margin:15}}>List Data</Text>
               
                {adik.map(item => (
                    <View style={styles.list}>
                        <Text key={id}>{`${item.id} - ${item.nama}`}</Text>
                        <Text>{item.alamat}</Text>
                        <TouchableOpacity onPress={() => this.handleDeleteData(item.id)}>
                            <Text style={{color:'deeppink'}}>Delete</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => this.handleEditData(item.id, item.nama, item.alamat)}>
                            <Text style={{color:'deeppink'}}>Edit</Text>
                        </TouchableOpacity>
                    </View>
                ))}
               
            </View>
        )
    }
}
 
export default StateFundamental
 
const styles = StyleSheet.create({
  list:{
    flexDirection:'row',
    justifyContent:'space-evenly'
  }
})