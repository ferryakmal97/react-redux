import React, { Component } from 'react'
import { ScrollView, StyleSheet, Text, View } from 'react-native'

class Component1 extends Component {
    render() { 
        const array = []

        return (
            <View style={{backgroundColor:"blue", padding:10, flex:1}}>
                <Text style={{color:"white",fontSize:18, alignSelf:"center"}}>Parent Component</Text>
            <ScrollView showsVerticalScrollIndicator={false}>
                <View style={Style.containerBox}>
                {Array(20).fill('box').map((items, i) => { 
                    return (
                        <View style={Style.box} key={i}>
                            <Text style={{alignSelf:"center"}}> {items} - {i + 1} </Text>
                        </View>
                    )
                })}
                </View>
            </ScrollView>               
            </View>
            
        )
    }
}

export default Component1;

const Style = StyleSheet.create({
    box: {
        backgroundColor: "yellow",
        height: 150,
        margin: 20,
        justifyContent: "center",
        width: '48%', 
        margin: '1%', 
        aspectRatio: 1,
    },
    containerBox: {
        flexDirection: "row",
        width: "100%",
        flexWrap:"wrap"
    }
})