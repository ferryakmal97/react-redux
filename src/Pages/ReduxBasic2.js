import React, {Component} from 'react'
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome5'
import { connect } from 'react-redux'
import { deleteData } from '../redux/action'

class ReduxBasic2 extends Component {

    handleDeleteData = (id) => {
        this.props.deleteData(id)
    }
    render(){
        return (
            <View>
                <Text style={{fontWeight:'bold', alignSelf:'center', fontSize:20}}>List Data From Redux in ReduxBasic2 JS</Text>
                {this.props.data.map((item,i) => {
                    return (
                        <View style={styles.container}>
                            <Text key={i} style={{width:'25%'}}> {` ${i+1} - ${item.name}`} </Text>
                            <Text style={{width:'25%'}}>{item.address}</Text>
                            <TouchableOpacity  onPress={()=>this.handleDeleteData(item.id)} >
                                <Icon name='trash-alt' size={20}/>
                            </TouchableOpacity>
                            <TouchableOpacity  onPress={()=>alert('bisa')} >
                                <Icon name='pencil-alt' size={20}/>
                            </TouchableOpacity>
                        </View>
                    )
                })}
            </View>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        data: state.students
    }
}

const mapDispatchToProps = (dispatch) => {
    return{
        deleteData : (id) => dispatch(deleteData(id))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ReduxBasic2)

const styles = StyleSheet.create({
    container:{
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems:'center',
        padding:10
    }
})