import React, { Component } from 'react'
import {View, Text, StyleSheet, Image} from 'react-native'

class MovieScreen extends Component {
    render(){
        return(
            <View style={{flex:1}}>
                <View style={{flexDirection:"row"}}>
                    <Image source={{ uri: "https://avatars3.githubusercontent.com/u/62781589?s=460&u=0f2f526b306795e2d392f225aea966c646f47ab8&v=4" }} style={{height: 100, width: 100 }} />
                    <View style={{marginHorizontal:10, justifyContent:'space-between', marginVertical:10, backgroundColor:"yellow", flex:1, borderRadius:10, paddingHorizontal:10}}>
                        <Text>Title</Text>
                        <View style={{flexDirection:"row", justifyContent:'space-between', backgroundColor:"yellow",}} >
                            <Text>1992</Text>
                            <Text>Label</Text>
                        </View>
                    </View>
                </View>
            </View>
        )
    }
}

export default MovieScreen

const styles = StyleSheet.create({
    
})