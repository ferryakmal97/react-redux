import React from 'react'
import { View, Text, Dimensions } from 'react-native'

const Gap = ({ width, height}) => {
    return (
        <View style={{width:width, height:height}} />
      
    )
}

export default Gap;
