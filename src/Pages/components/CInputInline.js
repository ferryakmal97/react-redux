//import liraries
import React, {Fragment, useState} from 'react';
import { View, StyleSheet, TextInput, Dimensions } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
 
const {width} = Dimensions.get('window')
// create a component
const CInputInline = (props, style) => {
 
    const [hide, setHide] = useState(true)
    const [focus, setFocus] = useState(false)
 
    const color = focus?'darkblue':props.color
    return (
        <View style={styles.container}>
            {/* <Text>{iconName}</Text> */}
            <View style={{backgroundColor:'transparent', top:5}}>
                <Icon name={props.iconName} size={props.iconSize} color={color} />
            </View>
            <View style={[{...style}, styles.inputSection, {borderBottomColor: color}]}>
                {!props.password && (
                    <TextInput {...props} onFocus={() => setFocus(true)} onBlur={() => setFocus(false)} />
                )}
                {props.password && (
                    <Fragment>
                        <TextInput {...props} onFocus={() => setFocus(true)} onBlur={() => setFocus(false)} secureTextEntry={hide} />
                        {hide && (
                            <Icon name='eye-slash' size={20} color={color} onPress={() => setHide(!hide)}/>
                        )}
                        {!hide && (
                            <Icon name='eye' size={20} color={color} onPress={() => setHide(!hide)}/>
                        )}
                    </Fragment>
                )}
            </View>
        </View>
    );
};
 
CInputInline.defaultProps = {
    iconName: 'user',
    iconSize: 35,
    color: 'black'
};
 
//make this component available to the app
export default CInputInline;
 
// define your styles
const styles = StyleSheet.create({
    container:{
        flexDirection:'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    inputSection:{
        backgroundColor:'transparent',
        flexDirection:'row',
        alignItems:'center',
        width:width/1.3,
        borderBottomWidth:2,
        marginHorizontal:15,
        justifyContent: 'space-between',
    },
});
 