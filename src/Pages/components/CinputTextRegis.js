import React from 'react'
import { Dimensions, StyleSheet, Text, TextInput, View } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome';

const { width } = Dimensions.get("window");
const CinputTextRegis = ({icon, Placeholder}) => {
    
    return (
        <View style={{
            flexDirection: "row", alignItems: "center", borderColor: "black",
            borderWidth: 2, paddingHorizontal:5,
        }} >
            {/* <View> */}
                <Icon name={icon} size={30} color="black" />
                <TextInput style={{color:"black", fontSize:16, marginHorizontal:10}} placeholder={Placeholder}/>
            {/* </View> */}
                {/* <Icon name="eye-slash" size={30} color="black" /> */}
        </View>
    )
}

export default CinputTextRegis;

const styles = StyleSheet.create({})
