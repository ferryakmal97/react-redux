import React from 'react'
import { View, Text, TextInput } from 'react-native'
import  Icon  from 'react-native-vector-icons/FontAwesome'

const CinputTextRegisAuth = ({Placeholder}) => {
    return (
        <View style={{ flexDirection: "row", borderColor: "black", borderWidth: 2, 
                 alignItems: "center",  paddingHorizontal: 5}}>
            <Icon name="key" size={30} color="black" />
            <View style={{marginLeft: 5,width: "90%", flexDirection: "row", alignItems: "center", justifyContent: "space-between",}}>
                <TextInput style={{color:"black", fontSize:16}} placeholder={Placeholder} secureTextEntry={true} />
                <Icon style={{marginRight:10}} name="eye-slash" size={30} color="black" />
            </View>
         </View>
    )
}

export default CinputTextRegisAuth;
