import React, { useState } from 'react'
import { Fragment } from 'react';
import { StyleSheet, Text, TextInput, View } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome';

const CinputText = ({ props, icon, Placeholder,}) => {
    const [hide, setHide] = useState(true)
    const [focus, setFocus] = useState(false)

    const color = focus?'darkblue':'black'
    return (
        <View style={{flexDirection:"row",  alignItems:"center", }}>
        <Icon name={icon} size={30} color="black" />
        <View style={{marginLeft:10,borderColor:"black",  borderBottomWidth:2, width:"90%", alignItems:"center", flexDirection:"row",}}>
                {!props.password && (
                    <TextInput {...props} style={{ color: "black", fontSize: 16, }} placeholder={Placeholder} onFocus={() => setFocus(true)} onBlur={() => setFocus(false)} />
                    )} 
                {props.password && (
                    <Fragment>
                        <TextInput {...props} style={{ color: "black", fontSize: 16, }} placeholder={Placeholder} onFocus={() => setFocus(true)} onBlur={() => setFocus(false)} secureTextEntry={hide} />
                        {hide && (
                            <Icon name="eye-slash" size={20} color="black" onPress={() => setHide(!hide)}/>
                        )}
                        {!hide && (
                            <Icon name='eye' size={20} color={color} onPress={() => setHide(!hide)}/>
                        )}
                    </Fragment>
                )}
        </View>
        </View>
    )
}

export default CinputText;

CinputText.defaultProps = {
    iconName: 'user',
    iconSize: 35,
    color: 'black'
}

const styles = StyleSheet.create({})
