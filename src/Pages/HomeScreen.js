import React, { Component } from 'react'
import {View, Text, StyleSheet, Button, TextInput, TouchableOpacity} from 'react-native'
import { add } from 'react-native-reanimated'
import Icon from 'react-native-vector-icons/FontAwesome5'
import { connect } from 'react-redux'
import { addData, deleteData, editData, updateData } from '../redux/action'

class HomeScreen extends Component {
    constructor(){
        super()
        this.state = {
            id:'',
            nama: '',
            address: '',
        }
    }

    handleDeleteData = (id) => {
        this.props.deleteData(id)
    }

    addStudents(){
        this.props.addData({
            id : Math.floor(Math.random()*100),
            name: this.state.nama,
            address: this.state.address
        })
    }

    handleEditData = (id, name, address) => {
        console.log(id,name,address)
        this.setState({
            id: id,
            nama: name,
            address: address
        })
    }

    updateStudents(){
        const {id,nama,address}= this.state
        console.log(this.state)
        this.props.updateData({id,nama,address})
    }
    
    render(){
        const {id, nama, address} = this.state
        return(
            <View>
                <View style={styles.inputan}>
                    <TextInput value={nama} placeholder='nama' onChangeText={(e)=>this.setState({
                        nama : e
                    })} />
                </View>
                <View style={styles.inputan}>
                    <TextInput value={address} placeholder='address' onChangeText={(e)=>this.setState({
                        address : e
                    })} />
                </View>
                <View style={styles.box}>
                    <TouchableOpacity style={styles.tombolan} onPress={()=>this.addStudents()} >
                        <View>
                            <Text>Tambah Data</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.tombolan}>
                        <View>
                            <Text onPress={()=>this.updateStudents()} >Update Data</Text>
                        </View>
                    </TouchableOpacity>
                </View>

                {/* looping */}
                <View>
                    <Text style={{fontWeight:'bold', alignSelf:'center', fontSize:20}}>List Data From Redux</Text>
                    {this.props.students.map((item,i) => {
                        return (
                            <View key={i} style={styles.container}>
                                <Text style={{width:'25%'}}> {` ${i+1} - ${item.name}`} </Text>
                                <Text style={{width:'25%'}}>{item.address}</Text>
                                <TouchableOpacity  onPress={()=>this.handleDeleteData(item.id)} >
                                    <Icon name='trash-alt' size={20}/>
                                </TouchableOpacity>
                                <TouchableOpacity  onPress={() => this.handleEditData(item.id, item.name, item.address)} >
                                    <Icon name='pencil-alt' size={20}/>
                                </TouchableOpacity>
                            </View>
                        )
                    })}
                </View>
            </View>
        )
    }
}

const mapStateToProps = (state) => {
    return{
        title: state.title,
        description: state.description,
        students: state.students
    }
}

const mapDispatchToProps = (dispatch) => {
    return{
        deleteData : (id) => dispatch(deleteData(id)),
        addData : (data) => dispatch(addData(data)),
        updateData : (data) => dispatch(updateData(data))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen)

const styles = StyleSheet.create({
    container:{
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems:'center',
        padding:10
    },
    inputan:{
        borderWidth:2,
        marginHorizontal:17,
        marginVertical:5,
        borderRadius:10
    },
    box:{
        flexDirection:'row',
        margin:10,
        paddingHorizontal:20,
        height:'8%',
        justifyContent:'space-between'
    },
    tombolan:{
        elevation:5,
        width:'48%',
        alignItems:'center',
        justifyContent:'center',
        backgroundColor:'lightblue',
        borderRadius:10
    }
})