import React from 'react'
import { View, Text } from 'react-native'
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import LoginScreen from '../Pages/LoginScreen';
import RegisterScreen from '../Pages/RegisterScreen';
import ReduxBasic from '../Pages/ReduxBasic'
import StateFundamental from '../stateprops/StateFundamental';
import ListRedux from '../Pages/ListRedux'
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs'
import Icon from 'react-native-vector-icons/FontAwesome5'
import HomeScreen from '../Pages/HomeScreen';
import MovieScreen from '../Pages/MovieScreen'

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

const Routes = () => {
    return (
        <NavigationContainer>
            <Tab.Navigator initialRouteName="Home" screenOptions={({ route }) => ({
                tabBarIcon: ({ focused, color, size }) => {
                    let iconName;

                    if (route.name === 'Home') {
                    iconName = focused
                        ? 'home'
                        : 'house-damage';
                    } else if (route.name === 'List') {
                    iconName = focused 
                        ? 'list-alt' 
                        : 'list';
                    } else {
                    iconName = focused 
                        ? 'film' 
                        : 'file-video';
                    }
                    // You can return any component that you like here!
                    return <Icon name={iconName} size={size} color={color} />;
                },
                })}>
                <Tab.Screen name="Home" component={HomeScreen}/>
                <Tab.Screen name="List" component={ListRedux}/>
                {/* <Tab.Screen name="StateFundamental" component={StateFundamental}/> */}
                <Tab.Screen name="Movie" component={MovieScreen}/>
            </Tab.Navigator>
        </NavigationContainer>
    )
}

export default Routes
