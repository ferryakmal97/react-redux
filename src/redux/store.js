import { create } from 'react-test-renderer';
import {createStore, applyMiddleware} from 'redux'
import thunk from 'redux-thunk';
import reducer from './reducer'

const Store = createStore(reducer,applyMiddleware(thunk))

export default Store