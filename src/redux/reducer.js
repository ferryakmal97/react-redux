const initialState = {
    students:[
        {id:1, name:'Gilang', address:'Kutoarjo'},
        {id:2, name:'Akmal', address:'Munich'},
        {id:3, name:'Ary', address:'Brebes'},
        {id:4, name:'Sukron', address:'Alas Roban'}
    ]
}

const reducer = (state = initialState, action) => {
    switch(action.type){
        case 'DELETE-DATA':
            return{
                ...state,
                students: state.students.filter(item => item.id != action.payload)
            }
        case 'CHANGE-TITLE':
            return{
                ...state,
                title:action.title
            }
        case 'ADD-DATA':
            return{
                ...state,
                students:[
                    ...state.students, action.payload
                ]
            }
        case 'UPDATE-DATA':
            return{
                ...state,
                students: state.students.map(item => {
                    if(item.id == action.payload.id){
                        return{
                            id: action.payload.id,
                            name: action.payload.nama,
                            address: action.payload.address
                        }
                    }
                    return item
                })
            }
        default:
            return state
    }
}

export default reducer