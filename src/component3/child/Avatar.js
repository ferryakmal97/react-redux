import React, { Component } from 'react'
import { View, Text } from 'react-native'

const Avatar = ({text}) => {
    return (
        <View style={{ alignItems:"center",justifyContent:"center",width: 70, height: 70, borderRadius: 50, backgroundColor: "red", alignSelf: "center", borderColor: "black", borderWidth: 2 }} >
            <Text style={{fontWeight:"bold"}}>{text}</Text>
       </View>
    )
}

export default Avatar;
