import React, { Component } from 'react'
import { View, Text, BackHandler } from 'react-native'

class Styling1 extends Component { 
    render() { 
        return (
            <View style={{backgroundColor:"blue", flex:1, paddingHorizontal:3}}>
                <Text style={{ color: "white", alignSelf: "center", fontSize: 20, marginTop: 10 }}>Parent Component</Text>
                <View style={{flexDirection:"row", marginTop:20}}>
                    <View style={{ backgroundColor: "purple", width: 150, height: 25, borderColor:'black', borderWidth:2}}></View>
                    <View  style={{backgroundColor:"green", width:150, height:25,borderColor:'black', borderWidth:2}}></View>
                </View>
                <View style={{flexDirection:"row", justifyContent:'space-between', marginTop:20}}>
                    <View style={{ backgroundColor: "purple", width: 150, height: 25, borderColor:'black', borderWidth:2}}></View>
                    <View  style={{backgroundColor:"green", width:150, height:25,borderColor:'black', borderWidth:2}}></View>
                </View>
                <View style={{ backgroundColor: "purple" , marginTop:30, padding:5,}}>
                    <View style={{flexDirection:"row", justifyContent:"space-between"}}>
                        <View style={{ backgroundColor: "green", width: 150, height: 25, borderColor:'black', borderWidth:2}}></View>
                        <View  style={{backgroundColor:"green", width:150, height:25,borderColor:'black', borderWidth:2}}></View>
                    </View>
                    <View style={{flexDirection:"row", marginTop:10}}>
                        <View style={{ backgroundColor: "green", width: 150, height: 25, borderColor:'black', borderWidth:2}}></View>
                        <View  style={{backgroundColor:"yellow", width:150, height:25,borderColor:'black', borderWidth:2}}></View>
                    </View>
                    <View style={{flexDirection:"row", marginTop:10, alignSelf:"center"}}>
                        <View style={{ backgroundColor:"yellow", width: 100, height: 25, borderColor:'black', borderWidth:2}}></View>
                        <View  style={{backgroundColor:"green", width:100, height:25,borderColor:'black', borderWidth:2}}></View>
                        <View  style={{backgroundColor:"red", width:100, height:25,borderColor:'black', borderWidth:2}}></View>
                    </View>
                </View>
                <View style={{backgroundColor:"purple", marginTop:10, padding:10, flexDirection:"row"}}>
                    <View style={{ height: 70, width: 70, backgroundColor: "yellow", borderRadius: 50 ,borderWidth:2}}></View>
                    <View style={{paddingHorizontal:10}}>
                        <View style={{ backgroundColor: "green", height: 30, width: 290 ,borderWidth:2}}></View>
                        <View style={{flexDirection:"row", marginTop:10}}>
                            <View style={{backgroundColor:"red", height:30, width:100, borderWidth:2}}></View>
                            <View style={{backgroundColor:"yellow", height:30, width:100, borderWidth:2, marginLeft:5}}></View>
                        </View>
                    </View>
                </View>
                <View style={{marginTop:10}} >
                    <View style={{backgroundColor:"green", width:300, height:30, borderWidth:2, marginTop:5}}></View>
                    <View style={{backgroundColor:"green", width:300, height:30, borderWidth:2, marginTop:5}}></View>
                    <View style={{backgroundColor:"green", width:300, height:30, borderWidth:2, marginTop:5}}></View>
                </View>
                <View style={{marginTop:10, alignSelf:"flex-end"}} >
                    <View style={{backgroundColor:"yellow", width:300, height:30, borderWidth:2, marginTop:5}}></View>
                    <View style={{backgroundColor:"yellow", width:300, height:30, borderWidth:2, marginTop:5}}></View>
                    <View style={{backgroundColor:"yellow", width:300, height:30, borderWidth:2, marginTop:5}}></View>
                </View>
            </View>
        )
    }
}
export default Styling1;
