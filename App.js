import React, { Component } from 'react';
import { Text, View } from 'react-native';
import { StyleSheet, StatusBar } from 'react-native';
import Component1 from './src/component1';
import Component2 from './src/component2';
import Component3 from './src/component3';
import LoginScreen from './src/Pages/LoginScreen';
import RegisterScreen from './src/Pages/RegisterScreen';
import Routes from './src/route';
import Styling1 from './src/Styling1';
import ReduxBasic from './src/Pages/ReduxBasic';

import {Provider} from 'react-redux'
import Store from './src/redux/store'

class App extends Component { 
  render(){
    return (
      <>
        {/* <Text>Styling React</Text> */}
        {/* <Styling1 /> */}
        {/* <Component1 /> */}
        {/* <Component3 /> */}
        {/* <LoginScreen /> */}
        {/* <RegisterScreen /> */}

        <Provider store={Store}>
          <StatusBar backgroundColor='transparent' barStyle='dark-content' />
          <Routes />
        </Provider>
        
      
      </>
    )
  }
}

export default App;

const style = StyleSheet.create({})